
let getCube = (a, b) => {
    console.log(`${a}^${b} is ${a**b}`)
};
getCube(2,3);

address = [258, "Washington Ave NW", "California", 90011];

const [strNum, street, state, zipCode] = address;

console.log(`I live at ${strNum} ${street}, ${state} ${zipCode}`);

let animal = {
    name: "Lolong",
    animalType: "saltwater crocodile",
    weight: 1075,
    measure: "20 ft 3 in"
}

const {name, animalType, weight, measure} = animal;

console.log(`${name} was a ${animalType}. He weighed at ${weight} kgs with a measurement of ${measure}.`)

let numArray = [1,2,3,5,7,11,13,17,19];

numArray.forEach(num => console.log(num));

let reduceNum = console.log(numArray.reduce((a,b) => a+b));
reduceNum;

class Dog {
    constructor(name, age, breed) {
        this.name = name;
        this.age = age;
        this.breed = breed;
    }
}

let dog = new Dog("Duke", 1, "Belgian Malinois");
console.log(dog);

